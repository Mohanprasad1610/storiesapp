require "test_helper"

class CommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @comment = comments(:one)
    @post = posts(:one)
  end



  test "should get new" do
    get "/posts/#{@post.id} /comments/new"
    assert_response :success
  end

  test "should create comment" do
    assert_difference('Comment.count') do
      post comments_url, params: { comment: { body: @comment.body, parent_id: @comment.parent_id, post_id: @comment.post_id } }
    end

    assert_redirected_to comment_url(Comment.last)
  end

end

