class Post < ApplicationRecord
  validates :title, :user, presence: true
  has_many :comments
end
